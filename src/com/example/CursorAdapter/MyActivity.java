package com.example.CursorAdapter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MyActivity extends Activity {


    ListView lv;
    MyCursorAdapter adapter;
    DBHelper dbHelper;
    SQLiteDatabase db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        dbHelper = new DBHelper(this);
        db = dbHelper.getReadableDatabase();
        Cursor c = getCursor();
        adapter = new MyCursorAdapter(this, c, 0);

        lv = (ListView) findViewById(R.id.listView);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, final long id) {
                MyViewHolder holder = (MyViewHolder) view.getTag();
                holder.btnDel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dbHelper.getWritableDatabase();
                        int delCount = db.delete(DataBaseConstants.TABLE, DataBaseConstants.ID + " = " + id, null);
                        Log.d("mylog", "position = " + position + ":   id = " + id);

                        ((MyCursorAdapter) lv.getAdapter()).changeCursor(getCursor());
                    }
                });
            }
        });
    }

    private Cursor getCursor() {
        Cursor c = db.query(DataBaseConstants.TABLE, null, null, null, null, null, null);
        return c;
    }

    class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            super(context, DataBaseConstants.BASE, null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL("create table " + DataBaseConstants.TABLE + " ("
                    + DataBaseConstants.ID + " integer primary key autoincrement,"
                    + DataBaseConstants.TITLE + " text,"
                    + DataBaseConstants.COORDS + " text,"
                    + DataBaseConstants.DESC + " text" + ");");

            ContentValues cv = new ContentValues();

            String[] titleArr = {"first test point", "Test 2"};
            String[] coordsArr = {"50.43943427891665;30.51212914288044", "50.439433263605473;30.512119419872764"};
            String[] descArr = {"Point description", "random text: h h on of bal-bla-bla"};

            for (int i = 0; i < titleArr.length; i++) {
                cv.put(DataBaseConstants.TITLE, titleArr[i]);
                cv.put(DataBaseConstants.DESC, descArr[i]);
                cv.put(DataBaseConstants.COORDS, coordsArr[i]);
                db.insert(DataBaseConstants.TABLE, null, cv);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }


}
