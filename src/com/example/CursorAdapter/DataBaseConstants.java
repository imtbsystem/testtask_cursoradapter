package com.example.CursorAdapter;

public class DataBaseConstants {

    public static String BASE = "myDB.sqlite";
    public static String TABLE = "testtable";
    public static String ID = "_id";
    public static String TITLE = "title";
    public static String DESC = "description";
    public static String COORDS = "coords";
}
