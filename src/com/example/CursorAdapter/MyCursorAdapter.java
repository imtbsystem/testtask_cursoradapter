package com.example.CursorAdapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class MyCursorAdapter extends CursorAdapter {

    private final Context context;

    public MyCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        this.context = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.list_item, parent, false);

        MyViewHolder holder = new MyViewHolder();
        holder.tvTitle = (TextView) v.findViewById(R.id.tvTitle);
        holder.tvCoord = (TextView) v.findViewById(R.id.tvCoord);
        holder.tvDesc = (TextView) v.findViewById(R.id.tvDesc);
        holder.btnDel = (ImageView) v.findViewById(R.id.btnDel);
        v.setTag(holder);

        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        MyViewHolder holder = (MyViewHolder) view.getTag();

        String title = cursor.getString(cursor.getColumnIndex(DataBaseConstants.TITLE));
        String coords = cursor.getString(cursor.getColumnIndex(DataBaseConstants.COORDS));
        String desc = cursor.getString(cursor.getColumnIndex(DataBaseConstants.DESC));

        holder.tvTitle.setText(title);
        holder.tvCoord.setText(coords);
        holder.tvDesc.setText(desc);

    }


}
